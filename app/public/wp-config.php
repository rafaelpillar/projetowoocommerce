<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'dyYQO1QUeewCfG50s6ppBjUnV4lEqKK1v6i+z13i+Me9gf9V/f6SKXm7cij6y70OkZy2GBz5VXNfRjpdc7Joqg==');
define('SECURE_AUTH_KEY',  '9noSiVymnVA0RBNKwlf9x1hq89ZtTkiZ6tXEdIK+QpHzcKgFqliTR0I8PgEW0RFRDZuGRT2YHF3lX8owGEPzrA==');
define('LOGGED_IN_KEY',    'jn8woOxgqNPc83ZeJmSIm8fzo9Im7LPCMI80HL6JPHcuiFp/L5Hb3xrAD4B6YN0+kMN592nOkTJa6/qa6AGYqw==');
define('NONCE_KEY',        'XOQVIXna5UyMe4BGliPP/ctxQ8PuPxSkfaHViQSh9xhbOV3V9s5A/nm6Zmzz+9mxNpLmkUq521aCa2n6rB+CCw==');
define('AUTH_SALT',        'M1mGVRUoTWBgqldJNgyEvCkpEhiqaoFwUhuQrrjPvfYWYGlkfiwkVa2om4xksn0DdhuAHhdgVdY+GvyK0y/E/A==');
define('SECURE_AUTH_SALT', '7SW5p46W+JvsLIwGnoLd04fVitlSYmchP51rq1JvWYrWgPc2HDGiIY0Yf5oWSL2POQezeJ66MxpPkfjh2CO0Gw==');
define('LOGGED_IN_SALT',   'cTKH3PGr2h/yxxiLIVpLeTWzdti+2rKtyN9r2X22gbm5u53gLZdbG4p0rhPrHHQZMNq+/Ezhn4p7+SbDccP/QA==');
define('NONCE_SALT',       '+wY0n/8dIFXIJr2//ATbA00Lq+QRPK3VpNzXAX0cgrTtADh3spgusKfCh8W/tF4+Bwqy8R9fgZQq3na8br9WbQ==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
